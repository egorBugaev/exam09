import React, { Component } from 'react';
import './App.css';
import {Route, Switch} from "react-router-dom";
import Contacts from "./component/Contacts/Contacts";
import AddContact from "./component/AddContact/AddContact";
import EditContact from "./component/EditContact/EditContact";


class App extends Component {
	render() {
		return (
			<Switch>
					<Route path="/" exact component={Contacts}/>
					<Route path="/add" render={(props) => <AddContact {...props}/>}/>
					<Route path="/edit" render={(props) => <EditContact {...props}/>}/>
					{/*<Route path="/checkout" component={Checkout}/>*/}
					<Route render={() => <h1>404 Not found</h1>}/>
				</Switch>
	
		);
	}

}

export default App;
