import React, {Component} from 'react'
import Form from "../Contacts/Form";

class AddContacts extends Component {
	cancel = () => {
		this.props.history.push('/');
	};
		render() {
				return (
				<Form cancel={this.cancel}/>
		);
	}
}

export default AddContacts;
