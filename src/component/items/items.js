import React from 'react';
import Item from "./item";

const Items = props => {
	return (
			<div className="AddItems__items">
				{
					props.items.map(item => {
						return <Item
							thisItem={item}
							key={item.key}
							edit={props.edit}
							remove={props.remove}
						/>
					})
				}
			</div>
	)
};

export default Items;