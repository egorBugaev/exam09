import React from 'react';

const Item = props => {
	return (
		<div className="AddItems__items--item" >
			<div className="AddItems__items--item--icon">
				<img src={props.thisItem.photo} alt=""/>
			</div>
			<div className="AddItems__items--item--descr">
				<div className="AddItems__items--item--name">{props.thisItem.name}</div>
				<div className="AddItems__items--item--name">{props.thisItem.key}</div>
				<div className="AddItems__items--item--name">{props.thisItem.phone}</div>
				<div className="AddItems__items--item--name">{props.thisItem.email}</div>
				<button onClick={()=>props.edit(props.thisItem.key)}>edit</button>
				<button	onClick={()=>props.remove(props.thisItem.key)}>remove</button>
			</div>
		</div>
	)
};

export default Item;