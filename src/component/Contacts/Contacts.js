import React, {Component, Fragment} from 'react'
import {connect} from 'react-redux';
import {editItem, removeItem, updateBook} from "../../store/actions";
import Header from "../Header/Header";
import Items from "../items/items";

class Contacts extends Component {
	componentDidMount() {
		this.props.updateBook();
	}
	addContact = () => {
		this.props.history.push('/add');
	};
	editContact = (id) => {
		this.props.editItem(id);
		this.props.history.push('/edit');
	};
	
	render() {
		let items = (
			<Items
				remove={this.props.removeItem}
				edit={this.editContact}
				items={this.props.list}
			/>
		);
		
		return (
			<Fragment>
				<Header
				addContact={this.addContact}/>
				{items}
			</Fragment>
			
		
		);
	}
}

const mapStateToProps = state => {
	return {
		list: state.list,
		id: state.id
	}
};

const mapDispatchToProps = dispatch => {
	return {
		updateBook: () => dispatch(updateBook()),
		removeItem: (id) => dispatch(removeItem(id)),
		editItem: (id) => dispatch(editItem(id))
	}
};


export default connect(mapStateToProps, mapDispatchToProps)(Contacts);
