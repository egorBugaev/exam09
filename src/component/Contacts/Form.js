import React, {Component, Fragment} from 'react'
import {updateBook} from "../../store/actions";
import connect from "react-redux/es/connect/connect";
import axios from "axios/index";
import img from './../../assets/img/img.jpg'


class Form extends Component {
	state={
		name:'',
		phone:'',
		email:'',
		photo: img,
	
	};
	changeInputHandler = (event) => {
		const name = event.target.name;
		this.setState({[name]: event.target.value});
	};
	componentDidMount() {
		console.log(this.props.person);
		if (!this.props.person) {
			this.setState(
				{
					name: this.props.person.name,
					phone: this.props.person.phone,
					email: this.props.person.email,
					photo: this.props.person.photo
				}
			)
		}
	}
	
	 addItem = () => {
			let item = {
				name: this.state.name,
				phone: this.state.phone,
				email: this.state.email,
				photo: this.state.photo
			};
			axios.post('/tbook.json', item).then(() => this.props.updateBook());
			this.props.cancel();
	};
	
	render(){
		
		return (
			<Fragment>
			<div className="FormRow">
				<label><span className="LabelName">Name:</span>
					<input type="text" name="name"
					       value={this.state.name}
					       onChange={(e) => this.changeInputHandler(e)}
					/>
				</label>
			</div>
			<div className="FormRow">
				<label><span className="LabelName">Phone:</span>
					<input type="number" name="phone"
					       value={this.state.phone}
					       onChange={(e) => this.changeInputHandler(e)}
					/>
				</label>
			</div>
			<div className="FormRow">
				<label><span className="LabelName">Email:</span>
					<input type="email" name="email"
					       value={this.state.email}
					       onChange={(e) => this.changeInputHandler(e)}
					/>
				</label>
			</div>
			<div className="FormRow">
				<label><span className="LabelName">Photo URL:</span>
					<input type="text" name="photo"
					       value={this.state.photo}
					       onChange={(e) => this.changeInputHandler(e)}
					/>
				</label>
			</div>
			<div className="FormRow">
				<img className='preview' src={this.state.photo} alt=""/>
			</div>
				<button onClick={this.addItem}>Save</button>
				<button onClick={this.props.cancel}>Cancel</button>
			</Fragment>
		)
	}
}

const mapStateToProps = state => {
	return {
		person: state.person,
		id: state.id
	}
};

const mapDispatchToProps = dispatch => {
	return {
		updateBook: () => dispatch(updateBook()),
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(Form);

