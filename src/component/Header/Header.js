import React from 'react'
import './header.css'

const Header = props =>{
			return (
			<div className={'header'}>
				<h3 className='title'>Contacts</h3>
				<button className='add-contact' onClick={props.addContact}>Add Contact</button>
			</div>
		)
		
};
export default Header