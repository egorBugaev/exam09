import React, {Component} from 'react'
import Form from "../Contacts/Form";
import {connect} from "react-redux";
import {removeItem,} from "../../store/actions";

class EditContact extends Component {
	cancel = () => {
		this.props.history.push('/');
	};
	render() {
		return (
			<Form
				cancel={this.cancel}
				person={this.props.person}
			 
			/>
		);
	}
}
const mapStateToProps = state => {
	return {
		person: state.person,
	}
};

const mapDispatchToProps = dispatch => {
	return {
		removeItem: (id) => dispatch(removeItem(id)),
	}
};


export default connect(mapStateToProps, mapDispatchToProps)(EditContact);

