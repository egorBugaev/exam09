import axios from "axios/index";

export const FETCH_TODO_REQUEST = 'FETCH_TODO_REQUEST';
export const FETCH_TODO_SUCCESS = 'FETCH_TODO_SUCCESS';
export const FETCH_TODO_ERROR = 'FETCH_TODO_ERROR';
export const INPUT = 'INPUT';
export const ADD = 'ADD';
export const RESET = 'RESET';
export const DELETE = 'DELETE';
export const EDIT = 'EDIT';

export const fetchTodoRequest = () => {
	return {type: FETCH_TODO_REQUEST};
};

export const fetchTodoSuccess = (list) => {
	return {type: FETCH_TODO_SUCCESS, list};
};

export const fetchTodoError = () => {
	return {type: FETCH_TODO_ERROR};
};
export const fetchTodoEdit = (person) => {
	return {type: EDIT, person};
};

export const updateBook = () => {
	return dispatch => {
		dispatch(fetchTodoRequest());
		axios.get('/tbook.json').then(response => {
			const list = [];
			if (response.data) {
				for (let key in response.data) {
					let item = {
						name: response.data[key].name,
						phone: response.data[key].phone,
						email: response.data[key].email,
						photo: response.data[key].photo,
						key: key
					};
					list.push(item);
				}
				dispatch(fetchTodoSuccess(list))
			};
		}, error => {
			dispatch(fetchTodoError());
		});
	}
};

export const removeItem = (id) => {
	console.log(id);
	return (dispatch) => {
		axios.delete(`/tbook/${id}.json`).then(() => {
			dispatch(updateBook())
		});
		dispatch({type: DELETE});
	}
};
export const editItem = (id) => {
	return (dispatch) => {
		axios.get(`/tbook/${id}.json`).then(response => {
			// console.log(response.data);
			dispatch(fetchTodoEdit(response.data))
		})
	}
};
