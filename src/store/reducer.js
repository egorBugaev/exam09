import {ADD, DELETE, EDIT, FETCH_TODO_SUCCESS, INPUT, RESET} from "./actions";

const initialState = {
	list:[],
	id:'',
	person:{}
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case FETCH_TODO_SUCCESS:
            return {...state, list: action.list};
		case INPUT:
			return {...state, inputValue: action.value};
		case EDIT:
			return {...state, person: action.person};
		case ADD:
			return {...state};
		case RESET:
			return {...state, inputValue: action.value};
		case DELETE:
			return {...state};
		default:
			return state;
	}
};

export default reducer;